'''
Created on Nov 29, 2018

@author: kkosturanov
'''
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class Functions():
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    def expl_click_wait_ID(self,idString):
        try:
            element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID,idString )))
            element.click()
        except:
            print("TOO LONG elem not found")
        pass
        
    def expl_insertText_ID(self,idString,inputText):
        try:
            element = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.ID,idString )))
            element.set_value(inputText)
        except:
            print("TOO LONG elem not found")
        pass
    
    def presence_ID(self,idString):
        try:
            element = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.ID,idString )))
            if (element.is_displayed()):
                print("Element with id" + idString + "is displayed")
                return element
        except:
            print("TOO LONG elem not found, element not found")     
     
        '''
        print("Explicitly waiting for element ", idString)
        return WebDriverWait(self.driver, wait_time).until\
        (expected_conditions.presence_of_element_located((By.ID,idString)))  
        
        '''
        