'''
Created on Oct 31, 2018

@author: kkosturanov
'''
import unittest
from appium import webdriver
from TestAndroid import Functions



class Test(unittest.TestCase):


    def setUp(self):
        #set up the env for the test
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0.1'
        desired_caps['deviceName'] = '5200f823e226c2e1'
        desired_caps['appPackage'] = 'it.h3g.areaclienti3'
        desired_caps['appActivity'] = 'it.h3g.areaclienti3.MainActivity'
        desired_caps['appium-version'] = '1.8.0'
        desired_caps['noReset'] = 'false'
        desired_caps['autoGrantPermissions'] = 'true'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        pass


    def tearDown(self):
        #tear down the test
        self.driver.quit()
        pass


    def testName(self):
        #self.driver.implicitly_wait(30)
        myFunctions = Functions.Functions
        
        print(self.driver.current_activity)
        
        if (self.driver.current_activity == '.SplashScreenActivity'):
            myFunctions.expl_click_wait_ID(self, "it.h3g.areaclienti3:id/closeTutorial")
            myFunctions.expl_click_wait_ID(self, "it.h3g.areaclienti3:id/checkBoxAlert")
            myFunctions.expl_click_wait_ID(self,"it.h3g.areaclienti3:id/buttonAlertTutorial")
            
        
        myFunctions.expl_click_wait_ID(self,"it.h3g.areaclienti3:id/showLoginBtn")
        
        print(self.driver.current_activity)
        
        self.assertIsNotNone(myFunctions.presence_ID(self,"it.h3g.areaclienti3:id/h3gLogo"))
        self.assertIsNotNone(myFunctions.presence_ID(self,"it.h3g.areaclienti3:id/loginTitle"))
        
        myFunctions.expl_insertText_ID(self,"it.h3g.areaclienti3:id/msisdnTxt",'3914371698')
        myFunctions.expl_insertText_ID(self,"it.h3g.areaclienti3:id/pwdTxt",'Pippo123')
        myFunctions.expl_click_wait_ID(self,"it.h3g.areaclienti3:id/pwdToggleVisibility")
        
        print("ANDROID APP")
        pass
    
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity=2).run(suite)